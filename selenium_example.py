import requests
from bs4 import BeautifulSoup
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions

# response = requests.get('https://thestoryshack.com/tools/gamertag-generator/')

# with open('test.txt', 'w') as f:
#     f.write(response.text)

# soup = BeautifulSoup(response.text, 'html.parser')
# items = soup.find_all('li', class_='card hide show')

# print(items)

# for i in items:
#     print(i.text)


driver = webdriver.Chrome()
driver.get('https://thestoryshack.com/tools/gamertag-generator/')

time.sleep(3)
list_items = driver.find_elements_by_css_selector(".card.hide.show")

gamertags = list(map(lambda element: element.text, list_items))

print('gamertags:', gamertags)
# print('gamertags[0]:', gamertags[0])

# # assert "Python" in driver.title
# # elem = driver.find_element_by_name("q")
# # elem.clear()
# # elem.send_keys("pycon")
# # elem.send_keys(Keys.RETURN)
# # assert "No results found." not in driver.page_source

driver.get('https://www.namecheap.com/domains/registration/results/')

wait = WebDriverWait(driver, 30)
wait.until(expected_conditions.presence_of_element_located(
    (By.XPATH, '//*[@id="react-nc-search"]/div/div/section/form/div/button[2]')))

search_box = driver.find_element_by_xpath('//*[@id="search-query"]')
search_box.clear()
search_box.send_keys(gamertags[0])
search_box.send_keys(Keys.RETURN)

time.sleep(2)
# driver.close()

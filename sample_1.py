import re
import json
import requests
from pprint import pprint


def download_image(image_name, image_url):
    with open(image_name, 'wb') as image_file:
        response = requests.get(image_url, stream=True)

        if not response.ok:
            print(response)

        for block in response.iter_content(1024):
            if not block:
                break

            image_file.write(block)


def parse_url(text):
    # # from stack overflow (https://stackoverflow.com/a/50568320/3897738)
    WEB_URL_REGEX = r"""(?i)\b((?:https?:(?:/{1,3}|[a-z0-9%])|[a-z0-9.\-]+[.](?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)/)(?:[^\s()<>{}\[\]]+|\([^\s()]*?\([^\s()]+\)[^\s()]*?\)|\([^\s]+?\))+(?:\([^\s()]*?\([^\s()]+\)[^\s()]*?\)|\([^\s]+?\)|[^\s`!()\[\]{};:'".,<>?«»“”‘’])|(?:(?<!@)[a-z0-9]+(?:[.\-][a-z0-9]+)*[.](?:com|net|org|edu|gov|mil|aero|asia|biz|cat|coop|info|int|jobs|mobi|museum|name|post|pro|tel|travel|xxx|ac|ad|ae|af|ag|ai|al|am|an|ao|aq|ar|as|at|au|aw|ax|az|ba|bb|bd|be|bf|bg|bh|bi|bj|bm|bn|bo|br|bs|bt|bv|bw|by|bz|ca|cc|cd|cf|cg|ch|ci|ck|cl|cm|cn|co|cr|cs|cu|cv|cx|cy|cz|dd|de|dj|dk|dm|do|dz|ec|ee|eg|eh|er|es|et|eu|fi|fj|fk|fm|fo|fr|ga|gb|gd|ge|gf|gg|gh|gi|gl|gm|gn|gp|gq|gr|gs|gt|gu|gw|gy|hk|hm|hn|hr|ht|hu|id|ie|il|im|in|io|iq|ir|is|it|je|jm|jo|jp|ke|kg|kh|ki|km|kn|kp|kr|kw|ky|kz|la|lb|lc|li|lk|lr|ls|lt|lu|lv|ly|ma|mc|md|me|mg|mh|mk|ml|mm|mn|mo|mp|mq|mr|ms|mt|mu|mv|mw|mx|my|mz|na|nc|ne|nf|ng|ni|nl|no|np|nr|nu|nz|om|pa|pe|pf|pg|ph|pk|pl|pm|pn|pr|ps|pt|pw|py|qa|re|ro|rs|ru|rw|sa|sb|sc|sd|se|sg|sh|si|sj|Ja|sk|sl|sm|sn|so|sr|ss|st|su|sv|sx|sy|sz|tc|td|tf|tg|th|tj|tk|tl|tm|tn|to|tp|tr|tt|tv|tw|tz|ua|ug|uk|us|uy|uz|va|vc|ve|vg|vi|vn|vu|wf|ws|ye|yt|yu|za|zm|zw)\b/?(?!@)))"""
    return re.findall(WEB_URL_REGEX, text)[0]


base_url = 'https://www.reddit.com'
subreddit_name = 'photoshopbattles'

subreddit_url = f'{base_url}/r/{subreddit_name}.json'

reddit_response = requests.get(subreddit_url, headers={'user-agent': 'D&A Reddit API Tutorials'})
json_data = reddit_response.json()

# with open('contests.json', 'w') as output_file:
#     output_file.write(json.dumps(json_data, indent=4))

contests = []

for post in json_data['data']['children']:
    post_data = post['data']

    if post_data['post_hint'] == 'image':
        contests.append({'title': post_data['title'], 'endpoint': post_data['permalink']})


# print(pprint(contests))

first_contest = contests[1]

submission_list_url = base_url + first_contest['endpoint'] + '.json'

# print(submission_list_url)

submission_list_response = requests.get(submission_list_url, headers={'user-agent': 'D&A Reddit API Tutorials'})
submission_page_data = submission_list_response.json()

# # with open('submissions.json', 'w') as submission_list_file:
# #     submission_list_file.write(json.dumps(submission_page_data, indent=4))

submissions = []

constest_image_url = submission_page_data[0]['data']['children'][0]['data']['url']

# print('constest_image_url:', constest_image_url)

download_image('0.jpg', constest_image_url)

submission_list_data = submission_page_data[1]['data']['children']

for item in submission_list_data:
    submission = item['data']

    if (item['kind'] == 't1' and submission['body'] != '[deleted]'):
        body = submission['body']

        try:
            url = parse_url(body)
            submissions.append(url)
        except:
            print('invalid:', body)

counter = 1

for submission in submissions:
    try:
        download_image(str(counter) + '.png', submission)
    except:
        print('Failed to download:', submission)

# print('submissions:', submissions)
